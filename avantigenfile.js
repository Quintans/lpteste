'use strict'

var gulp = require('gulp')
var $ = require('gulp-load-plugins')()
var inquirer = require('inquirer')
var updateNotifier = require('update-notifier')
var pkg = require('./package.json')

gulp.task('default', function(done) {
  updateNotifier({ pkg: pkg }).notify()

  var questions = [
    {
      name: 'storeName',
      message: 'Store account name: (Ex.: calvinklein)',
      default: 'store'
    },
    {
      name: 'storeAcronym',
      message: 'Store acronym: (Ex.: ckn)',
      default: 'acronym'
    },
    {
      type: 'list',
      name: 'storeTemplate',
      message: 'Select your template',
      choices: [
        {
          value: 'clean',
          name: 'Clean'
        },
        {
          value: 'express',
          name: 'Express'
        }
      ]
    },
    {
      type: 'list',
      name: 'newStore',
      message: 'Import vendors?',
      choices: [
        {
          value: true,
          name: 'Yes'
        },
        {
          value: false,
          name: 'No'
        }
      ]
    }
  ]

  inquirer.prompt(questions).then(function(answers) {
    answers.cleanTemplate = (answers.storeTemplate === 'clean') ? true : false;
    answers.expressTemplate = (answers.storeTemplate === 'express') ? true : false;

    gulp
      .src([answers.cleanTemplate ? 'template-clean/**' : 'template-express/**'], { cwd: __dirname, dot: true })
      .pipe($.template(answers, { interpolate: /<%=([\s\S]+?)%>/g }))
      .pipe(
        $.rename(function(file) {
          if (file.basename[0] === '@') {
            file.basename = '.' + file.basename.slice(1)
          }

          if (file.basename.indexOf('-acronym-') > -1) {
            file.basename = file.basename.replace('acronym', answers.storeAcronym)
          }
        })
      )
      .pipe($.conflict('./'))
      .pipe(gulp.dest('./'))
      .on('finish', function() {
        done()
      })
  })
})
