import "../../scss/0-<%= storeAcronym %>-web-styles.scss";

APP.controller.General = ClassAvanti.extend({
  init() {
    this.setup()
    this.start()
    this.bind()
  },

  setup() {
    
  },

  start() {
    
  },

  bind() {

  }
})
