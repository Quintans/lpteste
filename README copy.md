# Make sure to follow this guide to properly configure your project:

## Setup

### Installing

The first step you must follow to run the project is to install all its dependencies.

The following commands should do this for you.

```
yarn install
```

### Starting with Webpack
```
yarn dev (development environment)
yarn deploy (production environment)  
```

### AvantiClass and AvantiSearch

Currently, we have some problens with AvantiClass, AvantiSearch and Webpack (We are working on fix). One easy workaround is call the AvantiClass and AvantiSearch script directly on the HTML subtemplate 'scripts' **before** 'x-xxx-web-application.js' like this:

```
  <script src="/arquivos/avanti-class.js"></script>
  <script src="/arquivos/avanti-search.js"></script>
  ...
  <script src="/arquivos/x-xxx-web-application.js"></script>
```

### BrowserSync
  This webpack has BrowserSync it performs auto reloading after any changes to the file without the need for any software such as `Fiddler` to make this change in the production / development environment.
	 
```js
new BrowserSyncPlugin({
  open: 'external',
  https: true,
  ui: false,
  host: `${env.STORE_NAME}.vtexlocal.com.br`,
  startpath: '/admin/login/',
  proxy: `https://${env.STORE_NAME}.vtexcommercestable.com.br`,
  serveStatic: [
    {
      route: '/arquivos',
      dir: `${PATHS.dist}/arquivos`,
    },
  ],
}),
```

open `package.json`, inside him have some config:

This json is for you to put the name and acronym of your store. (**ONLY FOR YOUR KNOWLEDGE, STORE NAME AND ACRONYM IS AUTO GENERATED BY AVANTIGEN** )

```json
"scripts": {
  "dev": "webpack --config ./webpack/webpack.dev.js --watch --mode development --env.STORE_NAME={{ YOUR_STORE_NAME }} --env.STORE_ACRO={{ YOUR_STORE_ACRONYM }}",
  "deploy": "webpack --config ./webpack/webpack.prod.js --mode production --env.STORE_ACRO={{ YOUR_STORE_ACRONYM }}"
}, 
```

This variabel is responsible for making the exchange of files from your machine to `https://{{yourstore}}.vtexcommercestable.com.br/arquivos/` from vtex. Remembering that the files must have the same name.

After you have done the basic configuration for BrowserSync to work

**Starting development environment**

`yarn dev`

If everything is ok the terminal will show this message:

```js
[Browsersync] Proxying: https://{{yourStore}}.vtexcommercestable.com.br
[Browsersync] Access URLs:
 --------------------------------------------
  Local: https://localhost:3000
  External: https://{{yourStore}}.vtexlocal.com.br:3000
 --------------------------------------------
```

After you start the terminal it will open a link for you to login with your store credentials and after you have provided your store credentials you can develop everything in this url:

`https://{{yourStore}}.vtexlocal.com.br:3000/`
